//Watch project here : https://www.youtube.com/watch?v=be9sHQ7xqo0&t=1159s


const express = require('express');
const axios = require('axios');

const app = express();
const PORT = process.env.PORT || 5000;
// const apiKey = '0767e8ff1f15739a39a87e4f1305cab3';
//const baseUrl =`http://api.scraperapi.com`
//`${baesUrl}&url=https://www.amazon.com/dp/${productId}`

const generateScrapeUrl = (apiKey) => `http://api.scraperapi.com?api_key=${apiKey}&autoparse=true`;

app.use(express.json());


app.get('/', (req, res) => {
    res.send('HEY LOOK I AM AN API!!');
});

//GET PRODUCT DETAILS - ROUTE

//---FIRST GO TO MY SERVER path route
app.get('/product/:productId', (req, res) => {

    //Then GRAB THE PARAMS THROUGH DESTRUCTURING. WHATEVER IS NAMED IN productId inside params will be assigned to productId
    const { productId } = req.params;
    const { api_key } = req.query;



    //NOW MAKE A REQUEST TO AMAZON WITH AXIOS AS THE REQUEST CLIENT SO I CAN SCRAPE THE HTML AXIOS AUTOMATICALLY
    //TURNS INTO JSON AND GET IT BACK

    axios.get(`${generateScrapeUrl(api_key)}&url=https://www.amazon.com/dp/${productId}`)
        .then(response => {
            res.send(response.data);
        })
        .catch((error) => {
            console.log(error);
        })
});



//GET PRODUCT REVIEWS - ROUTE
app.get('/product/:productId/reviews', (req, res) => {
    const { productId } = req.params;
    const { api_key } = req.query;

    axios.get(`${generateScrapeUrl(api_key)}&url=https://www.amazon.com/product-reviews/${productId}`)
        .then(response => {
            res.send(response.data);
        })
        .catch((error) => {
            res.send('Error Happend');
        })

});





//GET OFFERS - ROUTE
app.get('/product/:productId/offers', (req, res) => {

    const { productId } = req.params;
    const { api_key } = req.query;

    axios.get(`${generateScrapeUrl(api_key)}&url=https://www.amazon.com/gp/offer-listing/${productId}`)
        .then((response) => {
            res.send(response.data)
        })
        .catch((error) => {
            res.send('Error Happend');
            console.log('OOPs! Something Happend ', error);
        })

});



//GET SEARCH QUERY - ROUTE
//: THIS MEANS VARIABLE!!  
app.get('/search/:searchQuery', (req, res) => {
    //READ IN PARAMETER
    const { searchQuery } = req.params;
    const { api_key } = req.query;

    axios.get(`${generateScrapeUrl(api_key)}&url=https://www.amazon.com/s?k=${searchQuery}`)
        .then(response => {

            res.send(response.data);
        })
        .catch(error => {
            res.send('Error Happend');
            console.log('Opps, and Error has occured!!!', error);
        })
});


app.get('/wind', (req, res) => {
    const { q } = req.params;
    const { api_key } = req.query;

    axios.get(`${generateScrapeUrl(api_key)}&url=https://www.google.com/search?rlz=1C5CHFA_enUS839US852&tbs=lf:1,lf_ui:9&tbm=lcl&sxsrf=ALeKk03__3yAtWBe2Wec52ty700l8of90Q:1629763185972&q=restaurants+near+me+open+now&rflfq=1&num=10&sa=X&ved=2ahUKEwjY0pH-rMjyAhXxdN8KHaYQCfcQjGp6BAgNEHY&biw=1680&bih=882#rlfi=hd:;si:;mv:[[40.772852199999996,-74.2447968],[40.728034699999995,-74.28739809999999]];tbs:lrf:!1m4!1u3!3m2!3m1!1e1!1m4!1u5!2m2!5m1!1sgcid_3italian_1restaurant!1m4!1u5!2m2!5m1!1sgcid_3pizza_1restaurant!1m4!1u2!2m2!2m1!1e1!1m4!1u1!2m2!1m1!1e1!1m4!1u1!2m2!1m1!1e2!1m4!1u16!2m2!16m1!1e1!1m4!1u16!2m2!16m1!1e2!1m4!1u22!2m2!21m1!1e1!2m1!1e2!2m1!1e5!2m1!1e16!2m1!1e1!2m1!1e3!3sCgIIASABKgJVUw,lf:1,lf_ui:9`)
        .then(response => {
            res.send(response.data)
        })

    // axios.get(`${generateScrapeUrl(api_key)}&https://www.google.com/search?q=${q}/`)
    //     .then(response => {
    //         res.send(response.data)
    //     })

})


app.listen(PORT, () => console.log(`Server running on ${PORT}`))



